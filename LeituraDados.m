%------------------------------------------------------------------
% Leitura (STM32F303K8) e impressao de dados 
%------------------------------------------------------------------
% Leituras do Acelerometro e do Giroscopio 
% 
% Fabián Barrera Prieto
% Mestrado em Sistemas Mecatrônicos
% 09/03/2017
%------------------------------------------------------------------
% function [values] = DadosTest ()
    
    close all;
    clear all;
    clc;

    oldobj = instrfind;     %elimina resquicios presentes na porta serial
    if not(isempty(oldobj)) 
        fclose(oldobj);     
        delete(oldobj);
    end
    
    if ~exist('s2','var')
        s2 = serial('COM16','BaudRate',9600,'Timeout',1);
    end
    if ~isvalid(s2)
        s2 = serial('COM16','BaudRate',9600,'Timeout',1);
    end
    if strcmp(get(s2,'status'),'closed')
        fopen(s2);
    end
    
    tic

    flag = 1;

    disp('En sus marcas. Posicione el sensor en la posición inicial')
    t_ini = toc; t_fin = 10; t = 0; 
    while (t < t_fin) %Espera 10 segundos para eliminar los datos iniciales
        t = toc - t_ini;
        fscanf(s2);
    end
    %pause(5);

%     tic
    t_fin = 10; t_ini = toc; i = 1; t = 0;
    while(t < t_fin) %Lee datos en un tiempo de 60 segundos
        if flag == 1
            flag = 0;
            disp('comienza')
        end

        str(i) = {fscanf(s2)}; %Almacena la cadena de string de los valores enviados por el Arduino
        i = i+1;

        t = toc - t_ini;
    end
    
    fclose(s2);
    n = length(str);
    c = 1;

    for i=5:n %3 para o programa no Arduino "accelGyro" e 8 para o programa no Arduino "MPU6050"
        tmp = eval(['[',str{i},'];']); %Selecciona un string para separarlo posteriormente en celdas
        if numel(tmp) == 7 %12 variáveis para armazenar
            values(c,:) = tmp; %Almacena y separa en diferentes celdas el string seleccionado 
            c = c + 1;
        end
    end
    
save DadosTest2 values

%-------------------------------------------------------------------------------------------------------------------------
%                                                       Figuras
%-------------------------------------------------------------------------------------------------------------------------
Nsamples = length(values);
dt = 0.01;
t = 0:dt:Nsamples*dt-dt;
%------------------------------------
% Acelerômetros RAW
%------------------------------------
figure;
plot(t, values(:,1), 'b') %ax
hold on
plot(t, values(:,2), 'r'); %ay
plot(t, values(:,3), 'g'); %az
title('Acelerômetros da MPU9250 sem calibração')
ylabel('aceleração (g)')
xlabel('Tempo (segundos)')
legend('ax', 'ay', 'az', 'Location','northeast','Orientation','horizontal')
% %------------------------------------
% % Acelerômetros calibrados
% %------------------------------------
% figure;
% plot(t, values(:,7), 'b') %ax
% hold on
% plot(t, values(:,8), 'r'); %ay
% plot(t, values(:,9), 'g'); %az
% title('Acelerômetros da MPU9250 calibrados')
% ylabel('aceleração (g)')
% xlabel('Tempo (segundos)')
% legend('ax', 'ay', 'az', 'Location','northeast','Orientation','horizontal')
% 
% %------------------------------------
% % Giroscópios RAW
% %------------------------------------
figure;
plot(t, values(:,4), 'b') %ax
hold on
plot(t, values(:,5), 'r'); %ay
plot(t, values(:,6), 'g'); %az
title('Giroscópios da MPU9250 sem calibração')
ylabel('Velocidade angular (°/s)')
xlabel('Tempo (segundos)')
legend('gx', 'gy', 'gz', 'Location','southeast','Orientation','horizontal')
% %------------------------------------
% % Giroscópios calibrados
% %------------------------------------
% figure;
% plot(t, values(:,10), 'b') %ax
% hold on
% plot(t, values(:,11), 'r'); %ay
% plot(t, values(:,12), 'g'); %az
% title('Giroscópios da MPU9250 calibrados')
% ylabel('Velocidade angular (°/s)')
% xlabel('Tempo (segundos)')
% legend('gx', 'gy', 'gz', 'Location','northeast','Orientation','horizontal')
% 

